FROM node:14.16

ADD --chown=1000:1000 src /home/node/app

WORKDIR /home/node/app

EXPOSE 8000

USER 1000

RUN npm i

ENV PORT=8000

CMD ["npm", "run", "start"]