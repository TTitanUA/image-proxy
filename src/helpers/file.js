const path = require('path')

module.exports.isImage = function (name) {
  switch (path.extname(name)) {
    case ".jpg":
    case ".jpeg":
    case ".png":
    case ".gif":
    case ".webp":
      return true;
  }

  return false;
}

module.exports.convertContentType = function (name, acceptWebp) {
  switch (path.extname(name)) {
    case ".png":
    case ".gif":
      return "image/png";
    case ".webp":
      return acceptWebp && "image/webp" || "image/png";
  }

  return "image/jpeg";
}

module.exports.convertFormat = function (name, acceptWebp) {
  switch (path.extname(name)) {
    case ".png":
    case ".gif":
      return "png";
    case ".webp":
      return acceptWebp && "webp" ||  "png";
  }

  return "jpeg";
}

module.exports.forceConvertContentType = function (format) {
  switch (format) {
    case "png":
      return "image/png";
    case "webp":
      return "image/webp";
  }

  return "image/jpeg";
}

module.exports.forceConvertFormat = function (format) {
  switch (format) {
    case "png":
      return "png";
    case "webp":
      return "webp";
  }

  return "jpeg";
}
