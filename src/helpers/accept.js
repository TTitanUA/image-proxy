
module.exports.acceptWebp = function (headers) {
  if(!headers.accept) {
    return false
  }

  return headers.accept.indexOf('image/webp') !== -1;
}
