const fetch = require('node-fetch');
const path = require('path')
const fs = require('fs')

const BASE_FS_DIR = '/storage/'

class DownloadService {
  getObject = async (uri, schema, base, prefix = '/') => {
    const resourcePath = path.join(prefix, uri)

    switch (schema) {
      case "http":
      case "https":
        return await this.fetchObject((new URL(resourcePath, schema + ':' + base)).toString());
      case "file":
        return await this.readObject(path.resolve(path.join(BASE_FS_DIR, resourcePath)));
    }

    return null;
  }

  fetchObject = async (url) => {
    try {
      let res = await fetch(url)
      if(res.status < 400) {
        return {
          Body: await res.buffer(),
        };
      }
    } catch (e) {
      // TODO: Error log
      return null;
    }
  }

  readObject = (path) => {
    return new Promise((resolve) => {
      fs.readFile(path, function (err, buffer) {
        if(err) {
          resolve(null)
        } else {
          resolve({
            Body: buffer
          })
        }
      })
    })
  }
}

module.exports = DownloadService;
