
const {acceptWebp} = require("../helpers/accept");
const {
  convertContentType,
  convertFormat,
  forceConvertContentType,
  forceConvertFormat,
} = require("../helpers/file");
const sharp = require('sharp');

class ResizeService {
  constructor({size, url, format}, headers) {
    this.acceptWebp = acceptWebp(headers);
    this.size = size;
    this.url = url;
    this.format = format;
  }

  resize = async (s3Object) => {

    return await sharp(s3Object.Body)
      .resize({
        ...this.getSize(),
        // fit: 'contain',
        fit: 'cover',
      })
      .toFormat(this.getFormat())
      .toBuffer()
  }

  getSize = () => {
    let size = {};
    const sizeParts = (this.size || '').split('x');

    if(sizeParts[0]) {
      size.width = parseInt(sizeParts[0]);
      if(size.width < 1) {
        size.width = undefined;
      }
    }

    if(sizeParts[1]) {
      size.height = parseInt(sizeParts[1]);
      if(size.height < 1) {
        size.height = undefined;
      }
    }

    return size;
  }

  getFormat = () => {
    if(this.format === 'webp' && this.acceptWebp) {
      return forceConvertFormat('webp');
    }

    return this.format !== 'webp' ? forceConvertFormat(this.format) : convertFormat(this.url, this.acceptWebp);
  }

  getContentType = () => {
    if(this.format === 'webp' && this.acceptWebp) {
      return forceConvertContentType('webp');
    }

    return this.format !== 'webp' ? forceConvertContentType(this.format) : convertContentType(this.url, this.acceptWebp);
  }
}

module.exports = ResizeService;
