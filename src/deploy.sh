#!/usr/bin/env bash


export PATH=/home/ubuntu/.nvm/versions/node/v14.15.0/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin

git pull origin master
yarn install
date > BUILD_ID
