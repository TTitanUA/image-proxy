const fp = require('fastify-plugin');
const querystring = require('querystring');

module.exports = fp(async (fastify) => {

  fastify.addHook('preHandler', (req, reply, done) => {
    const urlParts = (req.url.split('?'));

    const resizeParts = urlParts[0].split('@');
    const query = querystring.parse(urlParts[1] || '')

    req.resizeParams = {
      url: resizeParts[0],
      size: resizeParts[1],
      format: query.fmt || 'webp',
      scheme: req.headers['x-resize-scheme'] || null,
      base: req.headers['x-resize-base'] || null,
      prefix: req.headers['x-resize-prefix'] || '/',
    };


    done()
  })
});
