'use strict'

const {isImage} = require("../helpers/file");
const ResizeService = require("../services/resize");
const DownloadService = require("../services/download");

module.exports = async function (fastify, opts) {
  fastify.addSchema({
    $id: 'resizeHeader',
    properties: {
      'x-resize-scheme': {
        type: 'string',
        enum: ['http', 'https', 'file']
      },
      'x-resize-base': {
        type: 'string',
      },
      'x-resize-prefix': {
        type: 'string',
      }
    },
    required: ['x-resize-scheme']
  })

  fastify.addSchema({
    $id: 'resizeQuerystring',
    properties: {
      'fmt': {
        type: 'string',
        enum: ['jpeg', 'jpg', 'png', 'webp']
      }
    }
  })

  fastify.get('/*', {
    handler: async function (request, reply) {
      const {resizeParams, headers} = request;
      if (!isImage(resizeParams.url)) {
        reply.code(404).send()
      } else {
        await returnImage(fastify, reply, resizeParams, headers);
      }
    },
    schema: {
      headers: { $ref: 'resizeHeader#' },
      querystring: { $ref: 'resizeQuerystring#' }
    }
  })
}

async function returnImage(fastify, reply, resizeParams, headers) {
  try {
    const object = await (new DownloadService).getObject(
      resizeParams.url,
      resizeParams.scheme,
      resizeParams.base,
      resizeParams.prefix
    );

    if (object === null) {
      throw new Error('File not found');
    }

    const resizeService = new ResizeService(resizeParams, headers);
    return reply
    .type(resizeService.getContentType())
    .send(await resizeService.resize(object))

  } catch (e) {
    console.group("root.js:62 - returnImage");
    console.warn(e);
    console.groupEnd();
    return reply
      .header('Cache-Control', 'public, max-age=30')
      .code(404)
      .send()
  }
}
