'use strict'


module.exports = async function (fastify, opts) {
  fastify.register(require('./plugins/resize-params'))

  fastify.register(require('./routes/root'))
}
