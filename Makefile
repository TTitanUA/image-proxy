#!make
include .env
export $(shell sed 's/=.*//' .env)

PWD := $(shell pwd)
docker_image := image-proxy:latest
src_volume := $(PWD)/src:/home/node/app
example_volume := $(PWD)/example_storage:/storage/
port_proxy := $(DEV_PORT):8000
port_debug := $(DEV_DEBUG_PORT):9229

build:
	docker build -t $(docker_image) --no-cache .

dev: build
	docker run --rm -it --user=$(DEV_USER) -v $(src_volume) -v $(example_volume) -p $(port_proxy) -p $(port_debug) $(docker_image) bash

test:
	echo $(port_proxy)
